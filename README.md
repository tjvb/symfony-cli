# Symfony CLI image


## Example usage for the security:check in your GitLab CI.
```yaml
security:
  rules:
    # only run on the protected branches
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
    - when: never
  stage: check
  image: registry.gitlab.com/tjvb/symfony-cli:latest
  script:
    - symfony security:check --dir=$CI_PROJECT_DIR
  needs: [ ]
  dependencies: [ ]
```
