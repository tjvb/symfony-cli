FROM alpine as installer

RUN apk update && \
    apk upgrade && \
    apk add bash git wget

RUN wget https://get.symfony.com/cli/installer && \
    bash installer --install-dir=/usr/bin

FROM alpine

COPY --from=installer /usr/bin/symfony /usr/bin/symfony
